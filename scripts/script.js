var firstName = document.getElementById("firstname");
var email = document.getElementById("email");
var subject = document.getElementById("subject");
var message = document.getElementById("message");

window.onload = function() {

  if (document.getElementById("downloadEBook") != null)
    document.getElementById("downloadEBook").onclick = function() {
      location.href = "whitepaper.html";
    };
  if (document.getElementById("sendNow") != null)
    document.getElementById("sendNow").onclick = function() {
      location.href = "like-page.html";
    };
  if (document.getElementById("sendNowMain") != null)
    document.getElementById("sendNowMain").onclick = function() {
      let validate = validateform();
      if(validate) {
        let { getUserTracker } = window || {};
        if (getUserTracker && getUserTracker.addContactData) {
          // show loading here .

          // console.log(firstName.value + " " + email.value + " " + subject.value + " " + message.value);

          getUserTracker
            .addContactData({
              name: firstName.value,
              email: email.value,
              subject: subject.value,
              requirements: message.value
            })
            .then(resp => resp.json())
            .then(response => {
              //   console.log('resp', response)
              if (response.result === "done") {
                location.href = "thank-you.html";
              } else {
                  console.error(response.status, response.statusText)
              }
            })
            .catch(err => {
              console.error(err);
            });
        }
      }
    };
};

function validateform()
{
  // validation fails if the input is blank
  if(firstName.value == "") {
    alert("Error: First name is empty!");
    firstName.focus();
    return false;
  }

  if(email.value == "") {
    alert("Error: Email Id is empty!");
    email.focus();
    return false;
  }

  if(subject.value == "") {
    alert("Error: Subject is empty!");
    subject.focus();
    return false;
  }

  if(message.value == "") {
    alert("Error: Message is empty!");
    message.focus();
    return false;
  }

  // regular expression to match only alphanumeric characters and spaces
  var re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

  // validation fails if the input doesn't match our regular expression
  if(!re.test(email.value)) {
    alert("Error: Email is invalid!");
    email.focus();
    return false;
  }
  // validation was successful
  return true;
}